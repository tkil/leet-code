

const calcSum = (arr) => arr.reduce((acc, curr) => acc + curr, 0)

/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
export const combinationSum = function(candidates, target, isSub) {
  if(target <= 0) {
    return []
  }
  let arr = []
  for(const n of candidates) {
    const subArr = combinationSum(candidates, target - n);
    arr = [...arr, n, subArr];
    console.log(arr.flat())
    const sum = calcSum(arr.flat())
    if(sum === target) {
      return arr;
    }
  }
  return [];
  
  // return [[2,3]];
};
