import { combinationSum } from "./combinationSum";

describe("Module combinationSum", () => {
  describe("combinationSum()", () => {
    it("should return [[2,3]] for [2,3,4], target 5", () => {
      // Act
      const actual = combinationSum([2,3,4], 5);
      // Assert
      expect(actual).toEqual([[2,3]]);
    });
    it("should return [[2,2]] for [2,3], target 4", () => {
      // Act
      const actual = combinationSum([2,3], 4);
      // Assert
      expect(actual).toEqual([[2,2]]);
    });
    it("should return [[2,2,3],[7]] for [2,3,6,7], target 7", () => {
      // Act
      const actual = combinationSum([2,3,6,7], 7);
      // Assert
      expect(actual).toEqual([[2,2,3],[7]]);
    });
  });
});
