export const toBinaryStrRep = (num) => {
  return (num >>> 0).toString(2);
}

export const toDecimal = (str) => {
  return Number.parseInt(str, 2);
}

export const flipStrBin = (str) => {
  let flipped = "";
  for(const c of str) {
    flipped += c === "0" ? "1" : "0";
  }
  return flipped;
}

/**
 * @param {number} N
 * @return {number}
 */
export const bitwiseComplement = function(N) {
  const str = toBinaryStrRep(N);
  const flipped = flipStrBin(str);
  return toDecimal(flipped);
};