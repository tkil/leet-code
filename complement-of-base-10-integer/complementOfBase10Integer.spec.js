import { bitwiseComplement, flipStrBin, toBinaryStrRep, toDecimal } from "./complementOfBase10Integer"

describe("Complement of Base 10 Integer", () => {
  describe("toBinaryStrRep()", () => {
    it("should be 101 for 5", () => {
      // Act
      const actual = toBinaryStrRep(5);
      // Assert
      expect(actual).toEqual("101")
    })
  })
  describe("toDecimal()", () => {
    it("should be 5 for 101", () => {
      // Act
      const actual = toDecimal("101");
      // Assert
      expect(actual).toEqual(5)
    })
  })
  describe("flipStrBin()", () => {
    it("should be 010 for 101", () => {
      // Act
      const actual = flipStrBin("101");
      // Assert
      expect(actual).toEqual("010")
    })
  })
  describe("flipStrBin()", () => {
    it("should be 010100 for 101011", () => {
      // Act
      const actual = flipStrBin("101011");
      // Assert
      expect(actual).toEqual("010100")
    })
  })
  describe("bitwiseComplement()", () => {
    it("should be 5 for 2", () => {
      // Act
      const actual = bitwiseComplement(5)
      // Assert
      expect(actual).toEqual(2)
    })
    it("should be 0 for 7", () => {
      // Act
      const actual = bitwiseComplement(7)
      // Assert
      expect(actual).toEqual(0)
    })
    it("should be 10 for 5", () => {
      // Act
      const actual = bitwiseComplement(10)
      // Assert
      expect(actual).toEqual(5)
    })
  })
})