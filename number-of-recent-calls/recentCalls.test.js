import { RecentCounter } from "./recentCalls";

describe("Module recentCalls", () => {
  describe("RecentCounter()", () => {
    it("should return 1 for 1 ping", () => {
      // Arrange
      const recentCounter = new RecentCounter();
      // Act
      const actual = recentCounter.ping(1);
      // Assert
      expect(actual).toEqual(1);
    });
    it("should return 2 for 2 pings", () => {
      // Arrange
      const recentCounter = new RecentCounter();
      // Act
      recentCounter.ping(1);
      const actual = recentCounter.ping(2);
      // Assert
      expect(actual).toEqual(2);
    });
    it("should return 3 for 3 pings", () => {
      // Arrange
      const recentCounter = new RecentCounter();
      // Act
      recentCounter.ping(1);
      recentCounter.ping(2);
      const actual = recentCounter.ping(3);
      // Assert
      expect(actual).toEqual(3);
    });
    it("should return 3 for 3 pings within 3000ms", () => {
      // Arrange
      const recentCounter = new RecentCounter();
      // Act
      recentCounter.ping(1);
      recentCounter.ping(2000);
      const actual = recentCounter.ping(3001);
      // Assert
      expect(actual).toEqual(3);
    });
    it("should return 3 out of 4 pings within 3000ms", () => {
      // Arrange
      const recentCounter = new RecentCounter();
      // Act
      recentCounter.ping(1);
      recentCounter.ping(3000);
      recentCounter.ping(3001);
      const actual = recentCounter.ping(3002);
      // Assert
      expect(actual).toEqual(3);
    });
    it("should return 3 out of 5 pings within 3000ms", () => {
      // Arrange
      const recentCounter = new RecentCounter();
      // Act
      recentCounter.ping(1);
      recentCounter.ping(2);
      recentCounter.ping(3001);
      recentCounter.ping(3002);
      const actual = recentCounter.ping(3003);
      // Assert
      expect(actual).toEqual(3);
    });
  });
});
