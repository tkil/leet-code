export var RecentCounter = function() {
    
};

/** 
 * @param {number} t
 * @return {number}
 */
RecentCounter.prototype.ping = function(t) {
  const range = 3000;
  this.pings = [ ...(this.pings || []), t ]
  const recentPings = this.pings.filter((p) => (t - p <= range))
  return recentPings.length;
};

/** 
 * Your RecentCounter object will be instantiated and called as such:
 * var obj = new RecentCounter()
 * var param_1 = obj.ping(t)
 */