import { isIntervalCovered, removeCoveredIntervals } from "./removeCoveredIntervals"

describe("Remove Covered Intervals", () => {
  describe("isIntervalCovered()", () => {
    it("should return true for [3,6], [2,8]", () => {
      // Act
      const actual = isIntervalCovered([3,6], [2,8])
      // Assert
      expect(actual).toEqual(true)
    })
    it("should return false for [3,6], [5,8]", () => {
      // Act
      const actual = isIntervalCovered([3,6], [5,8])
      // Assert
      expect(actual).toEqual(false)
    })
    it("should return false for [1,2], [1,2]", () => {
      // Act
      const actual = isIntervalCovered([1,2], [1,2])
      // Assert
      expect(actual).toEqual(false)
    })
  })
  describe('removeCoveredIntervals()', () => {
    it('should be 2 for [[1,4],[3,6],[2,8]]', () => {
      // Act
      const actual = removeCoveredIntervals([[1,4],[3,6],[2,8]])
      // Assert
      expect(actual).toEqual(2)
    })
    it('should be 1 for [[1,4],[2,3]]', () => {
      // Act
      const actual = removeCoveredIntervals([[1,4],[2,3]])
      // Assert
      expect(actual).toEqual(1)
    })
    it('should be 2 for [[0,10],[5,12]]', () => {
      // Act
      const actual = removeCoveredIntervals([[0,10],[5,12]])
      // Assert
      expect(actual).toEqual(2)
    })
    it('should be 2 for [[3,10],[4,10],[5,11]]', () => {
      // Act
      const actual = removeCoveredIntervals([[3,10],[4,10],[5,11]])
      // Assert
      expect(actual).toEqual(2)
    })
    it('should be 1 for [[1,2],[1,4],[3,4]]', () => {
      // Act
      const actual = removeCoveredIntervals([[1,2],[1,4],[3,4]])
      // Assert
      expect(actual).toEqual(1)
    })
  })
})