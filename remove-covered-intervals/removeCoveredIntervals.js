
/**
 * @param {number[]} intervalA
 * @param {number[]} intervalB
 * @return {boolean}
 */
export const isIntervalCovered = (intervalA, intervalB) => {
  if(intervalB[0] === intervalA[0] && intervalB[1] === intervalA[1]){
    return false;
  }
  return intervalB[0] <= intervalA[0] && intervalB[1] >= intervalA[1];
}

/**
 * @param {number[][]} intervals
 * @return {number}
 */
export const removeCoveredIntervals = function(intervals) {
  let leanIntervals = 0;
  for(const current of intervals) {
    let isRedundant = false;
    for(const compare of intervals) {
      if(isIntervalCovered(current, compare)) {
        isRedundant = true
      }
    }
    if(!isRedundant) {
      leanIntervals++;
    }
  }
  return leanIntervals;
};