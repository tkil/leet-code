module.exports = {
  clearMocks: true,
  collectCoverage: false,
  collectCoverageFrom: [
    // Match Pattern
    "**/*.{js,jsx,ts,tsx}",
    // Ignore Directories
    "!**.expected.**",
    "!**/__tests__/**",
    "!**/assets/**",
    "!**/bin/**",
    "!**/cli/**",
    "!**/coverage/**",
    "!**/dist/**",
    "!**/env/**",
    "!**/node_modules/**",
    "!**/tests/**",
    "!**/types/**",
    "!**/vendor/**",
    // Ignore Files
    "!**/**.config.**"
  ],
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: ["/cli/", "/node_modules/"],
  moduleFileExtensions: ["js", "jsx", "ts", "tsx", "vue", "json"],
  // setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "@env/(.*)": "<rootDir>/env/$1",
    "@modules/(.*)": "<rootDir>/node_modules/$1"
  },
  modulePathIgnorePatterns: [],
  testMatch: [
    "**/*.(spec|test).+(js|jsx|ts|tsx)"
  ],
  testPathIgnorePatterns: ["/node_modules/"],
  transformIgnorePatterns: ["<rootDir>/node_modules/"],
  transform: {
    "^.+\\.jsx?$": "babel-jest"
  }
};
