export const arrayToArrayDiff = (nums, k) => {
  return nums.map((n) => n - k)
}

export const deDupe = (nums) => {
  return nums.reduce((acc, curr) => {
    if(acc.indexOf(curr) === -1) {
      acc.push(curr);
    }
    return acc;
  }, []);
}

export const isDupeNum = (nums, n) => nums.filter((d) => d === n).length > 1;

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
export const findPairs = function(nums, k) {
  let diffCount = 0;
  const numNoDupe = deDupe(nums);
  for(const n of numNoDupe) {
    // For diff zero we care about dupes
    if(k === 0) {
      if(isDupeNum(nums, n)) {
        diffCount++;
      }
    } else {
      const diffs = arrayToArrayDiff(numNoDupe, n);
      const numDiff = diffs.filter((d) => d === k).length;
      diffCount += numDiff;
    }
  }
  return diffCount;
};