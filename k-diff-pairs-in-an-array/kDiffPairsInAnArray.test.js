import { arrayToArrayDiff, deDupe, findPairs, isDupeNum } from "./kDiffPairsInAnArray";

describe("Problem kDiffPairsInAnArray", () => {

  describe('arrayToArrayDiff()', () => {
    it('should return [0, 1, 2] for [1, 2, 3], 1', () => {
      // Act
      const actual = arrayToArrayDiff([1, 2, 3], 1);
      // Assert
      expect(actual).toEqual([0, 1, 2])
    })
    it('should return [-1, 0, 1] for [1, 2, 3], 2', () => {
      // Act
      const actual = arrayToArrayDiff([1, 2, 3], 2);
      // Assert
      expect(actual).toEqual([-1, 0, 1])
    })
    it('should return [-2, -3, -4] for [-1, -2, -3], -3', () => {
      // Act
      const actual = arrayToArrayDiff([-1, -2, -3], -3);
      // Assert
      expect(actual).toEqual([2, 1, 0])
    })
    it('should return [0,-2,1,-2,2] for [3,1,4,1,5], 3', () => {
      // Act
      const actual = arrayToArrayDiff([3,1,4,1,5], 3);
      // Assert
      expect(actual).toEqual([0,-2,1,-2,2])
    })
  })
  describe('deDupe()', () => {
    it('should return [1, 2, 3] for [1, 2, 2, 3]', () => {
      // Act
      const actual = deDupe([1, 2, 2, 3]);
      // Assert
      expect(actual).toEqual([1, 2, 3]);
    })
  })
  describe('isDupeNum()', () => {
    it('should be true for [1, 2, 1]', () => {
      // Act
      const actual = isDupeNum([1, 2, 1], 1);
      // Assert
      expect(actual).toEqual(true)
    })
    it('should be true for [1, 2, 3]', () => {
      // Act
      const actual = isDupeNum([1, 2, 3], 1);
      // Assert
      expect(actual).toEqual(false)
    })
  })
  describe("findPairs()", () => {
    it("should return 2 for [1,2,3], 1", () => {
      // Act
      const actual = findPairs([1,2,3], 1);
      // Assert
      expect(actual).toEqual(2);
    });
    it("should return 1 for [1,2,3], 2", () => {
      // Act
      const actual = findPairs([1,2,3], 2);
      // Assert
      expect(actual).toEqual(1);
    });
    it("should return 2 for [3,1,4,1,5], 2", () => {
      // Act
      const actual = findPairs([3,1,4,1,5], 2);
      // Assert
      expect(actual).toEqual(2);
    });
    it("should return 4 for [1,2,3,4,5], 1", () => {
      // Act
      const actual = findPairs([1,2,3,4,5], 1);
      // Assert
      expect(actual).toEqual(4);
    });
    it("should return 1 for [1,3,1,5,4], 0", () => {
      // Act
      const actual = findPairs([1,3,1,5,4], 0);
      // Assert
      expect(actual).toEqual(1);
    });
    it("should return 2 for [1,2,4,4,3,3,0,9,2,3], 3", () => {
      // Act
      const actual = findPairs([1,2,4,4,3,3,0,9,2,3], 3);
      // Assert
      expect(actual).toEqual(2);
    });
    it("should return 2 for [-1,-2,-3], 1", () => {
      // Act
      const actual = findPairs([-1,-2,-3], 1);
      // Assert
      expect(actual).toEqual(2);
    });
  });
});
