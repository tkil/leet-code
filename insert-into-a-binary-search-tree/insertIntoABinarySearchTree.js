/**
 * @param {number} val
 * @param {any} root
 * @return {number}
 */
export const compareToRoot = (val, root) => {
  if (val > root.val) {
    return 1;
  } else if (val < root.val) {
    return -1;
  }
  return 0;
};

export const createNewLeaf = (num) => {
  return {
    val: num,
    left: null,
    right: null,
  };
};

/**
 * @param {any} root
 * @param {number} val
 * @return {any}
 */
export const insertIntoBST = function (root, val) {
  if (root === null) {
    return createNewLeaf(val);
  }
  switch (compareToRoot(val, root)) {
    case -1:
      if (root.left === null) {
        root.left = createNewLeaf(val);
      } else {
        root.left = insertIntoBST(root.left, val);
      }
      break;
    case 1:
      if (root.right === null) {
        root.right = createNewLeaf(val);
      } else {
        root.right = insertIntoBST(root.right, val);
      }
      break;
  }
  return root;
};
