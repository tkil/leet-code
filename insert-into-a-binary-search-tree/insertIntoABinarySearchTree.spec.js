import { compareToRoot, createNewLeaf, insertIntoBST } from "./insertIntoABinarySearchTree";

describe("Insert into a Binary Search Tree", () => {
  describe("createNewLeaf()", () => {
    it("should create a new leaf with a value 1", () => {
      // Act
      const actual = createNewLeaf(1);
      // Assert
      const expected = {
        val: 1,
        left: null,
        right: null,
      };
      expect(actual).toEqual(expected);
    });
    it("should create a new leaf with a value 3", () => {
      // Act
      const actual = createNewLeaf(3);
      // Assert
      const expected = {
        val: 3,
        left: null,
        right: null,
      };
      expect(actual).toEqual(expected);
    });
  });
  describe("compareToRoot()", () => {
    it("should be 1 for 4, {val: 3}", () => {
      // Arrange
      const val = 4;
      const root = { val: 3, left: null, right: null };
      // Act
      const actual = compareToRoot(val, root);
      // Assert
      expect(actual).toEqual(1);
    });
    it("should be -1 for 2, {val: 3}", () => {
      // Arrange
      const val = 2;
      const root = { val: 3, left: null, right: null };
      // Act
      const actual = compareToRoot(val, root);
      // Assert
      expect(actual).toEqual(-1);
    });
    it("should be 0 for 2, {val: 2}", () => {
      // Arrange
      const val = 2;
      const root = { val: 2, left: null, right: null };
      // Act
      const actual = compareToRoot(val, root);
      // Assert
      expect(actual).toEqual(0);
    });
  });
  describe("insertIntoBST()", () => {
    it("should do nothing", () => {
      // Arrange
      const val = 1;
      const root = {
        val: 1,
        left: null,
        right: null,
      };
      // Act
      const actual = insertIntoBST(root, val);
      // Assert
      expect(actual).toEqual(root);
    });
    it("should insert into empty root", () => {
      // Arrange
      const val = 1;
      const root = null;
      // Act
      const actual = insertIntoBST(root, val);
      // Assert
      const expected = {
        val: 1,
        left: null,
        right: null,
      };
      expect(actual).toEqual(expected);
    });
    it("should insert into left", () => {
      // Arrange
      const val = 1;
      const root = {
        val: 2,
        left: null,
        right: null,
      };
      // Act
      const actual = insertIntoBST(root, val);
      // Assert
      const expected = {
        val: 2,
        left: {
          val: 1,
          left: null,
          right: null,
        },
        right: null,
      };
      expect(actual).toEqual(expected);
    });
    it("should insert into right", () => {
      // Arrange
      const val = 3;
      const root = {
        val: 2,
        left: null,
        right: null,
      };
      // Act
      const actual = insertIntoBST(root, val);
      // Assert
      const expected = {
        val: 2,
        left: null,
        right: {
          val: 3,
          left: null,
          right: null,
        },
      };
      expect(actual).toEqual(expected);
    });
    it("should deep insert into left", () => {
      // Arrange
      const val = 1;
      const root = {
        val: 3,
        left: {
          val: 2,
          left: null,
          right: null,
        },
        right: null,
      };
      // Act
      const actual = insertIntoBST(root, val);
      // Assert
      const expected = {
        val: 3,
        left: {
          val: 2,
          left: {
            val: 1,
            left: null,
            right: null,
          },
          right: null,
        },
        right: null,
      };
      expect(actual).toEqual(expected);
    });
    it("should deep insert into right", () => {
      // Arrange
      const val = 5;
      const root = {
        val: 3,
        left: null,
        right: {
          val: 4,
          left: null,
          right: null,
        },
      };
      // Act
      const actual = insertIntoBST(root, val);
      // Assert
      const expected = {
        val: 3,
        left: null,
        right: {
          val: 4,
          left: null,
          right: {
            val: 5,
            left: null,
            right: null,
          },
        },
      };
      expect(actual).toEqual(expected);
    });
  });
});
